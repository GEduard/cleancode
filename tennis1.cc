#include "tennis.h"

static const int minimumPointsToWin = 4;

std::string getDisplayabelScoreWhenTie(int score)
{
	switch (score)
	{
	case 0:
		return "Love-All";
	case 1:
		return "Fifteen-All";
	case 2:
		return "Thirty-All";
	default:
		return "Deuce";

	}
}

bool isMinimumPointsToWinReached(int firstPlayerScore, int secondPlayerScore)
{
	return firstPlayerScore >= minimumPointsToWin || secondPlayerScore >= minimumPointsToWin;
}

bool isAdvantage(int firstPlayerScore, int secondPlayerScore)
{
	bool differenceBetweenPlayerScoreIsOne = abs(firstPlayerScore - secondPlayerScore) == 1;
	return isMinimumPointsToWinReached(firstPlayerScore, secondPlayerScore) && differenceBetweenPlayerScoreIsOne;
}

bool isWin(int firstPlayerScore, int secondPlayerScore)
{
	bool differenceBetweenPlayerScoreIsTwo = abs(firstPlayerScore - secondPlayerScore) >= 2;
	return isMinimumPointsToWinReached(firstPlayerScore, secondPlayerScore) && differenceBetweenPlayerScoreIsTwo;
}

std::string getDisplayableScoreWhenWin(int firstPlayerScore, int secondPlayerScore)
{
	if (firstPlayerScore > secondPlayerScore)
		return "Win for player1";
	return "Win for player2";
}

std::string getDisplayableScoreWhenAdvantage(int firstPlayerScore, int secondPlayerScore)
{
	if (firstPlayerScore > secondPlayerScore)
		return "Advantage player1";
	return "Advantage player2";
}

std::string scoreToString(int score)
{
	switch (score)
	{
	case 0:
		return "Love";
	case 1:
		return "Fifteen";
	case 2:
		return "Thirty";
	case 3:
		return "Forty";
	default:
		return "";
	}
}

bool isTie(int firstPlayerScore, int secondPlayerScore)
{
	return firstPlayerScore == secondPlayerScore;
}

std::string getDisplayableScore(int firstPlayerScore, int secondPlayerScore)
{
	return scoreToString(firstPlayerScore) + "-" + scoreToString(secondPlayerScore);
}

const std::string tennis_score(int firstPlayerScore, int secondPlayerScore) 
{
	if (isTie(firstPlayerScore,secondPlayerScore))
	{
		return getDisplayabelScoreWhenTie(firstPlayerScore);
	}
	if (isAdvantage(firstPlayerScore, secondPlayerScore))
	{
		return getDisplayableScoreWhenAdvantage(firstPlayerScore, secondPlayerScore);
	}
	if(isWin(firstPlayerScore, secondPlayerScore))
	{
		return getDisplayableScoreWhenWin(firstPlayerScore, secondPlayerScore);
	}
	return getDisplayableScore(firstPlayerScore, secondPlayerScore);
}
